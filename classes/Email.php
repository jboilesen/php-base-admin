<?php
	/*
	 * PBA - Copyright (c) 2011-2019 Jonathan Nunes Boilesen
	 *
	 *
	 * This software is Open Software.
	 *	This software is licensed under Apache License 2.0.
	 *
	 *
	 * author: Jonathan Nunes Boilesen
	 * date: 08/01/2016
	 */
	class Email{
		const table = "app.email";

		private $id;
		private $subject;
		private $html_body;
		private $text_body;
		private $created_at;

		public function __construct($id=null){
			if (is_int($id) && $id>0){
				$sql = "SELECT * FROM ".Email::table." WHERE id = :id";
				$email = Database::execute($sql, array("id"=> $id))->fetch();
				if ($email){
					$this->setId($email["id"]);
					$this->setSubject($email["subject"]);
					$this->setHtmlBody($email["html_body"]);
					$this->setTextBody($email["text_body"]);
					$this->setCreatedAt($email["created_at"]);
				}
			}
		}

		public static function create($obj){
			if (isset($obj["subject"]) &&
					isset($obj["html_body"]) &&
					isset($obj["text_body"]) &&
					strlen($obj["subject"]) > 0 &&
					strlen($obj["html_body"]) > 0 &&
					strlen($obj["text_body"]) > 0){
				$email = new Email();

				$email->setSubject($obj["subject"]);
				$email->setHtmlBody($obj["html_body"]);
				$email->setTextBody($obj["text_body"]);
				$data = array(
					"subject" => $email->getSubject(),
					"html_body" => $email->getHtmlBody(),
					"text_body" => $email->getTextBody()
				);

				$sql = "INSERT INTO ".Email::table."(subject, html_body, text_body) VALUES (:subject, :html_body, :text_body) RETURNING id";
				$result = Database::execute($sql, $data);
				if ($result){
					$email->setId(intval($result->fetch()["id"]));
					return $email;
				}
			}
			return false;
		}

		public function getId(){
			return $this->id;
		}
		private function setId($id){
			$this->id = $id;
		}

		public function getSubject(){
			return $this->subject;
		}
		public function setSubject($subject){
			$this->subject = $subject;
		}

		public function getHtmlBody(){
			return $this->html_body;
		}
		public function setHtmlBody($html_body){
			$this->html_body = $html_body;
		}

		public function getTextBody(){
			return $this->text_body;
		}
		public function setTextBody($text_body){
			$this->text_body = $text_body;
		}

		public function getCreatedAt(){
			return $created_at;
		}
		public function setCreatedAt($created_at){
			$this->created_at = new DateTime($created_at);
		}

	}
?>
