<?php
	/*
	 * PBA - Copyright (c) 2011-2019 Jonathan Nunes Boilesen
	 *
	 *
	 * This software is Open Software.
	 *	This software is licensed under Apache License 2.0.
	 *
	 *
	 * author: Jonathan Nunes Boilesen
	 * date: 08/01/2016
	 */
	class Permission extends Scope{
		const table = "app.permission";

		const USER_READ = 1;
		const USER_UPDATE = 2;

		const USER_MANAGEMENT_CREATE = 3;
		const USER_MANAGEMENT_READ = 4;
		const USER_MANAGEMENT_UPDATE = 5;

		const EMAIL_MANAGEMENT_CREATE = 6;
		const EMAIL_MANAGEMENT_READ = 7;
		const EMAIL_MANAGEMENT_UPDATE = 8;

		private $id;
		private $name;
		private $description;

		public function __construct($id=null){
			if ($id != null && $id > 0){
				$sql = "SELECT * FROM ".Permission::table." WHERE id = :id";
				$permission = Database::execute($sql, array("id" => $id))->fetch();
				$this->setId($permission["id"]);
				$this->setName($permission["name"]);
				$this->setDescription($permission["description"]);
				parent::__construct($permission["id_scope"]);
			}
		}

		public function getScope(){
			return parent;
		}

		public function getId(){
			return $this->id;
		}
		public function getName(){
			return $this->name();
		}
		public function getDescription(){
			return $this->description();
		}
	}
?>
