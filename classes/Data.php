<?php
	/*
	 * PBA - Copyright (c) 2011-2019 Jonathan Nunes Boilesen
	 *
	 *
	 * This software is Open Software.
	 *	This software is licensed under Apache License 2.0.
	 *
	 *
	 * author: Jonathan Nunes Boilesen
	 * date: 08/01/2016
	 */
	class Data {
		private $reference_class;
		private $object_id;

		public function instantiateObject(){
			if ($this->getReferenceClass()->getId() && $this->getObjectId()){
				return $this->getReferenceClass()->instantiate($this->getObjectId());
			}
		}

		public function render($field){
			if ($field != null && strlen($field) > 2){
				$object = $this->instantiateObject();
				return $this->getReferenceClass()->render($object, $field);
			}
			return false;
		}

		public static function handler($reference_class, $method, $params, $field){
			$obj = $reference_class->call($method, $params);
			if ($obj && $obj->getId()) return $reference_class->render($obj, $field);

			return false;
		}

		public function setReferenceClass($reference_class){
			$this->reference_class = new ReferenceClass($reference_class);
		}
		public function getReferenceClass(){
			return $this->reference_class;
		}

		public function setObjectId($object_id){
			$this->object_id = $object_id;
		}
		public function getObjectId(){
			return $this->object_id;
		}
	}
?>
