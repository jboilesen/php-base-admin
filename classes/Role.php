<?php
	/*
	 * PBA - Copyright (c) 2011-2019 Jonathan Nunes Boilesen
	 *
	 *
	 * This software is Open Software.
	 *	This software is licensed under Apache License 2.0.
	 *
	 *
	 * author: Jonathan Nunes Boilesen
	 * date: 08/01/2016
	 */
	class Role{
		const table = "app.role";
		const permission = "app.role_permission";

		const ADMINISTRATOR = 1;

		private $id;
		private $name;
		private $description;
		private $created_at;

		public function __construct($id){
			if (is_int($id) && $id>0){
				$sql = "SELECT * FROM ".Role::table." WHERE id = :id";
				$role = Database::execute($sql, array("id" => $id))->fetch();
				$this->setId($role["id"]);
				$this->setName($role["name"]);
				$this->setDescription($role["description"]);
				$this->setCreatedAt($role["created_at"]);
			}
		}

		public static function find(){
			$sql = "SELECT id FROM ".Role::table." ORDER BY name";
			$roles = Database::execute($sql)->fetchAll();
			$i = 0;
			$roles_list = array();

			try{
				foreach ($roles as $role){
					$roles_list[$i] = new Role($role["id"]);
					$i++;
				}
			}catch(Exception $e){
				Log::error("PBA [500] FATAL: Could not find system roles ".$e->getMessage());
			}
			return $roles_list;
		}

		private function setId($id){
			$this->id = $id;
		}
		public function getId(){
			return $this->id;
		}

		public function setName($name){
			$this->name = $name;
		}
		public function getName(){
			return $this->name;
		}

		public function setDescription($description){
			$this->description = $description;
		}
		public function getDescription(){
			return $this->description;
		}

		public function setCreatedAt($created_at){
			$this->created_at = new Datetime($created_at);
		}
		public function getCreatedAt(){
			return $this->created_at;
		}

		public function isPermitted($permission){
			if ($this->getId() && $permission && $permission>0){
				$sql = "SELECT count(id) FROM ".Role::permission." WHERE id_role = :id_role AND id_permission = :id_permission";
				$role_permission = Database::execute($sql, array("id_role" => $this->getId(), "id_permission" => $permission))->fetch()["count"];
				if ($role_permission && $role_permission == 1) return true;
			}
			return false;
		}
	}
?>
