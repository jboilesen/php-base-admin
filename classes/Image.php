<?php
	/*
	 * PBA - Copyright (c) 2011-2019 Jonathan Nunes Boilesen
	 *
	 *
	 * This software is Open Software.
	 *	This software is licensed under Apache License 2.0.
	 *
	 *
	 * author: Jonathan Nunes Boilesen
	 * date: 08/01/2016
	 */
	class Image{
		const table = "app.image";

		private $id;
		private $title;
		private $description;
		private $filename;
		private $created_at;

		public function __construct($id=null){
			if (is_int($id) && $id>0){
				$sql = "SELECT * FROM ".Image::table." WHERE id = :id";
				$image = Database::execute($sql, array("id" => $id))->fetch();
				$this->setId($image["id"]);
				$this->setTitle($image["title"]);
				$this->setDescription($image["description"]);
				$this->setFilename($image["filename"]);
				$this->setCreatedAt($image["created_at"]);
			}
		}

		/*
		 * jQuery DataTables Plugin
		 */
		public static function dataTable($request){
			$columns = array(
				0 => "title",
				1 => "description",
				2 => "created_at",
				3 => "action"
			);

			$order_by = "";
			if (count($request["order"])>0){
				$i = 0;
				$order_by = " ORDER BY ";
				foreach ($request["order"] as $order){
					if ($i>0) $order_by.=",";
					$order_by.= $columns[$order["column"]]." ".$order["dir"];
					$i++;
				}
			}

			if ($request["search"]["value"]!='') {
				$sql = "SELECT id, title, description, to_char(created_at,'DD/MM/YYYY HH24:MI:SS') as created_at FROM ".Image::table." WHERE title LIKE :search OR description LIKE :search OR to_char(created_at,'DD/MM/YYYY HH24:MI:SS') LIKE :search $order_by LIMIT :limit OFFSET :offset";
				$data = array("search" => '%'.$request["search"]["value"].'%', "limit" => $request["length"], "offset" => $request["start"]);

				$sql_filtered = "SELECT count(id) FROM ".Image::table." WHERE title LIKE :search OR description LIKE :search OR to_char(created_at,'DD/MM/YYYY HH24:MI:SS') LIKE :search";
				$data_filtered = array("search" => '%'.$request["search"]["value"].'%');
			}else{
				$sql = "SELECT id, title, description, to_char(created_at,'DD/MM/YYYY HH24:MI:SS') as created_at FROM ".Image::table." $order_by LIMIT :limit OFFSET :offset";
				$data = array("limit" => $request["length"], "offset" => $request["start"]);

				$sql_filtered = "SELECT count(id) FROM ".Image::table;
				$data_filtered = null;
			}

			$images = Database::execute($sql, $data)->fetchAll();

			$filtered = Database::execute($sql_filtered, $data_filtered)->fetch()["count"];

			$i = 0;
			$images_list = array();
			foreach ($images as $image){
				$images_list[$i][0] = $image["title"];
				$images_list[$i][1] = $image["description"];
				$images_list[$i][2] = $image["created_at"];
				$images_list[$i][3] = '<div class=""><div class="btn-group"><button name="image-view" id="image_view_'.$image["id"].'" value="/images/view/'.$image["id"].'" class="link table-action btn btn-default"><i class="fa fa-search"></i></button><button name="image-edit" id="image_edit_'.$image["id"].'" value="/images/edit/'.$image["id"].'" class="link table-action btn btn-default"><i class="fas fa-pencil-alt"></i></button></div></div>';
				$i++;
			}

			$total = Database::execute("SELECT count(image.id) FROM ".Image::table)->fetch()["count"];

			$response = array(
				"draw" => intval($request["draw"]),
				"recordsTotal" => $total,
				"recordsFiltered" => $filtered,
				"data" => $images_list
			);
			return $response;
		}

		public static function find(){
			$sql = "SELECT id FROM ".Image::table." ORDER BY title";
			$images = Database::execute($sql)->fetchAll();
			$i = 0;
			$images_list = array();
			foreach ($images as $image) {
				$images_list[$i] = new Image($image["id"]);
				$i++;
			}
			return $images_list;
		}

		public static function findFilename($filename){
			$sql = "SELECT id FROM ".Image::table." WHERE filename = :filename";
			$images = Database::execute($sql, array("filename" => $filename))->fetchAll();
			$i = 0;
			$images_list = array();
			foreach ($images as $image) {
				$images_list[$i] = new Image($image["id"]);
				$i++;
			}
			return $images_list;
		}

		/*
		 * image Create
		 */
		public static function create($obj, $file){
			/* Check if object is valid */
			if (isset($obj['title']) &&
				isset($obj['description']) &&
				Image::validate($obj['title'],$obj['description'])){
				/* Creating new image */
				$image = new Image();
				$image->setTitle($obj['title']);
				$image->setDescription($obj['description']);
				/* Save file */
				try{
					$filename = StringUtils::sanitize(strtolower($image->getTitle())).'.'.pathinfo($file->getClientFilename(), PATHINFO_EXTENSION);
					$image->setFilename($filename);
					$file->moveTo($_ENV["PBA_IMAGE_UPLOAD_PATH"].$image->getFilename());

					/* Insert new image into database */
					$data = array("title" => $image->getTitle(), "description" => $image->getDescription(), "filename" => $image->getFilename());
					$sql = "INSERT into ".Image::table."(title, description, filename) VALUES (:title, :description, :filename) RETURNING id";
					$id =  Database::execute($sql, $data)->fetch()["id"];

					if (is_int($id) && $id > 0){
						$image->setId($id);
						$dimensions = getimagesize($_ENV["PBA_IMAGE_UPLOAD_PATH"].$image->getFilename());
						try {
							for ($i = 1; $i < 3; $i++){
								$alt_file[$i] = str_replace(".".pathinfo($image->getFilename(), PATHINFO_EXTENSION), "x".$i.".".pathinfo($image->getFilename(), PATHINFO_EXTENSION), $image->getFilename());
								$imagick = new Imagick($_ENV["PBA_IMAGE_UPLOAD_PATH"].$image->getFilename());
								$imagick->scaleImage(floor($dimensions["0"]*($i/3)), floor($dimensions["1"]*($i/3)), true);
								$imagick->writeImage($_ENV["PBA_IMAGE_UPLOAD_PATH"].$alt_file[$i]);
							}
						} catch (Exception $e){
							Log::error("PBA [500] FATAL: Error creating alternative images scales | ".$e->getMessage());
						}
						return $image;
					}
				}catch(Exception $e){
					Log::error("PBA [500] FATAL: Error creating image ".$_ENV["PBA_IMAGE_UPLOAD_PATH"].$filename." | ".$e->getMessage());
				}
			}
			return false;
		}

		/*
		 * image Update
		 */
		public function update(){
			if (Image::validate($this->getTitle(), $this->getDescription())){
				$data = array(
					"title" => $this->getTitle(),
					"description" => $this->getDescription(),
					"id" => $this->getId()
				);

				$sql = "UPDATE ".Image::table." SET title = :title, description = :description WHERE id = :id";

				return Database::execute($sql, $data);
			}
			return false;
		}

		/*
		 * image Validate
		 */
		public static function validate($title, $description){
			if ($title!=null && strlen($title)>2 && $description!=null && strlen($description)>5) return true;

			return false;
		}

		private function setId($id){
			$this->id = $id;
		}
		public function getId(){
			return $this->id;
		}

		public function setTitle($title){
			$this->title = $title;
		}
		public function getTitle(){
			return $this->title;
		}

		public function setDescription($description){
			$this->description = $description;
		}
		public function getDescription(){
			return $this->description;
		}

		public function setFilename($filename){
			$this->filename = $filename;
		}
		public function getFilename(){
			return $this->filename;
		}

		public function getLowResolution(){
			return str_replace(".".pathinfo($this->getFilename(), PATHINFO_EXTENSION), "x1.".pathinfo($this->getFilename(), PATHINFO_EXTENSION), $_ENV["PBA_ADMIN_IMAGE_FOLDER"].rawurlencode($this->getFilename()));
		}
		public function getMidResolution(){
			return str_replace(".".pathinfo($this->getFilename(), PATHINFO_EXTENSION), "x2.".pathinfo($this->getFilename(), PATHINFO_EXTENSION), $_ENV["PBA_ADMIN_IMAGE_FOLDER"].rawurlencode($this->getFilename()));
		}
		public function getHighResolution(){
			return $_ENV["PBA_ADMIN_IMAGE_FOLDER"].rawurlencode($this->getFilename());
		}

		public function setCreatedAt($created_at){
			$this->created_at = new Datetime($created_at);
		}
		public function getCreatedAt(){
			return $this->created_at;
		}

		public function toArray(){
			return array(
				"id" => $this->getId(),
				"title" => $this->getTitle(),
				"description" => $this->getDescription(),
				"high_resolution" => $this->getHighResolution(),
				"created_at" => $this->getCreatedAt()
			);
		}
	}
?>
