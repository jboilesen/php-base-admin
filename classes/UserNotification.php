<?php
	/*
	 * PBA - Copyright (c) 2011-2019 Jonathan Nunes Boilesen
	 *
	 *
	 * This software is Open Software.
	 *	This software is licensed under Apache License 2.0.
	 *
	 *
	 * author: Jonathan Nunes Boilesen
	 * date: 08/01/2016
	 */
	class UserNotification {
		const table = "app.user_notification";

		private $id;
		private $user;
		private $notification;
		private $email_user;
		private $read;
		private $created_at;

		public function __construct($id=null){
			if (is_int($id) && $id>0){
				$sql = "SELECT * FROM ".UserNotification::table." WHERE id = :id";
				$user_notification = Database::execute($sql, array("id" => $id))->fetch();
				$this->setId($user_notification["id"]);
				$this->setUser($user_notification["id_user"]);
				$this->setNotification($user_notification["id_notification"]);
				$this->setEmailUser($user_notification["id_email_user"]);
				$this->setRead($user_notification["read"]);
				$this->setCreatedAt($user_notification["created_at"]);
			}
		}

		public static function create($obj){
			if (isset($obj["user"]) &&
				isset($obj["notification"]) &&
				UserNotification::validate($obj["user"], $obj["notification"])){

				$user_notification = new UserNotification();

				$user_notification->setUser(intval($obj["user"]));

				$user_notification->setNotification(intval($obj["notification"]));

				$user_notification->setEmailUser(null);
				if (isset($obj["email_user"]) && $obj["email_user"]!=null) $user_notification->setEmailUser(intval($obj["email_user"]));

				$data = array(
					"id_user" => $user_notification->getUser()->getId(),
					"id_notification" => $user_notification->getNotification()->getId(),
					"id_email_user" => $user_notification->getEmailUser()->getId()
				);

				$sql = "INSERT INTO ".UserNotification::table."(id_user, id_notification, id_email_user) VALUES (:id_user, :id_notification, :id_email_user) RETURNING ID";
				$id = Database::execute($sql, $data);

				if ($id){
					$user_notification->setId($id->fetch()["id"]);
					return $user_notification;
				}
			}
			return false;
		}

		public static function validate($user, $notification){
			$user = new User(intval($user));
			if (!$user->getId()) return false;

			$notification = new Notification(intval($notification));
			if (!$notification->getId()) return false;

			return true;
		}

		public function read(){
			if (isset($_SESSION["user"]) && $_SESSION["user"]->getId() == $this->getUser()->getId()){
				$sql = "UPDATE ".UserNotification::table." SET read = 't' WHERE id = :id";
				return Database::execute($sql, array("id" => $this->getId()));
			}
			return false;
		}

		private static function add($notification, $user, $data, $email_user=null){
			$id_email_user = null;
			if ($email_user && $email_user->getId()) $id_email_user = $email_user->getId();

			$user_notification = UserNotification::create(array("notification" => $notification->getId(), "user" => $user->getId(), "email_user" => $id_email_user));

			if ($user_notification && $user_notification->getId() && is_array($data) && count($data)>0) {
				foreach($data as $obj){
					$reference_class = ReferenceClass::get($obj);
					$input = array(
						"id_user_notification" => $user_notification->getId(),
						"id_reference_class" => $reference_class->getId(),
						"object_id" => $obj->getId()
					);
					if (!UserNotificationData::create($input)) Log::error("PBA [500]: Failed to create user notification data");
				}
				return true;
			}
			return false;
		}

		public static function event($id_notification, $data){
			if ($id_notification && is_array($data) && count($data)>0){
				$notification = new Notification($id_notification);
				if ($notification->getId()){
					$users = User::getSubscribedToNotification($notification);
					if (is_array($users) && count($users)>0){
						foreach ($users as $user){
							$email_user = null;
							/* Create email user for email notifications */
							if ($notification->getEmailTemplate() && $notification->getEmailTemplate()->getId()){
								$email_data = $data;
								array_push($email_data, $user);
								$email_user = EmailUser::addToQueue($notification->getEmailTemplate()->getId(), $user, $email_data);
							}
							if (!UserNotification::add($notification, $user, $data, $email_user)) Log::error("PBA [500] Notice: could not add user notification");
						}
						return true;
					}
				}
			}
			return false;
		}

		public function getData(){
			if ($this->getId() && $this->getNotification() && $this->getNotification()->getId()){
				$id_reference_class = null;
				switch ($this->getNotification()->getId()){
					case Notification::new_page:
						$id_reference_class = ReferenceClass::Page;
					break;
					case Notification::new_click:
						$id_reference_class = ReferenceClass::Click;
					break;
					case Notification::new_hook:
						$id_reference_class = ReferenceClass::Hook;
					break;
					case Notification::new_lead:
					case Notification::lead_activity:
					case Notification::new_lead_contact:
						$id_reference_class = ReferenceClass::Lead;
					break;
				}

				$reference_class = new ReferenceClass(intval($id_reference_class));
				if ($reference_class->getId()){
					$data = UserNotificationData::get($this, $reference_class);
					if ($data && $data->getId()) return $data->instantiateObject();
				}
			}
			return false;
		}


		public function getId(){
			return $this->id;
		}
		private function setId($id){
			$this->id = $id;
		}

		public function getUser(){
			return $this->user;
		}
		public function setUser($user){
			$this->user = new User($user);
		}

		public function getNotification(){
			return $this->notification;
		}
		public function setNotification($notification){
			$this->notification = new Notification($notification);
		}

		public function getEmailUser(){
			return $this->email_user;
		}
		public function setEmailUser($email_user){
			$this->email_user = new EmailUser($email_user);
		}

		public function getRead(){
			return (bool)$this->read;
		}
		public function setRead($read){
			$this->read = (bool)$read;
		}

		public function getCreatedAt(){
			return $this->created_at;
		}
		public function setCreatedAt($created_at){
			$this->created_at = new Datetime($created_at);
		}
	}
?>
