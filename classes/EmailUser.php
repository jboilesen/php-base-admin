<?php
	/*
	 * PBA - Copyright (c) 2011-2019 Jonathan Nunes Boilesen
	 *
	 *
	 * This software is Open Software.
	 *	This software is licensed under Apache License 2.0.
	 *
	 *
	 * author: Jonathan Nunes Boilesen
	 * date: 08/01/2016
	 */
	class EmailUser{
		const table = "app.email_user";
		const email_confirmation = 1;
		const reset_password = 2;

		private $id;
		private $email_template;
		private $user;
		private $sent;
		private $sent_at;
		private $parse_success;
		private $error;
		private $created_at;

		public function __construct($id=null){
			if (is_int($id) && $id>0){
				$sql = "SELECT * FROM ".EmailUser::table." WHERE id = :id";
				$email_user = Database::execute($sql, array("id"=> $id))->fetch();
				if ($email_user){
					$this->setId($email_user["id"]);
					$this->setEmailTemplate($email_user["id_email_template"]);
					$this->setUser($email_user["id_user"]);
					$this->setSent($email_user["sent"]);
					$this->setSentAt($email_user["sent_at"]);
					$this->setParseSuccessful($email_user["parse_successful"]);
					$this->setError($email_user["error"]);
					$this->setCreatedAt($email_user["created_at"]);
				}
			}
		}

		public static function create($obj){
			if (isset($obj["email_template"]) &&
				isset($obj["user"])){
				$email_user = new EmailUser();
				$email_user->setEmailTemplate(intval($obj["email_template"]));
				$email_user->setUser(intval($obj["user"]));

				if ($email_user->getEmailTemplate()->getId() && $email_user->getUser()->getId()){
					$data = array(
						"id_email_template" => $email_user->getEmailTemplate()->getId(),
						"id_user" => $email_user->getUser()->getId()
					);
					$sql = "INSERT INTO ".EmailUser::table."(id_email_template, id_user) VALUES (:id_email_template, :id_user) RETURNING id";
					$result = Database::execute($sql, $data);
					if ($result){
						$email_user->setId(intval($result->fetch()["id"]));
						return $email_user;
					}
				}
			}
			return false;
		}

		public function update(){
			if (EmailUser::validate($this->getEmailTemplate()->getId(), $this->getUser()->getId())){
				$sent_at = null;
				if ($this->getSentAt()) $sent_at = $this->getSentAt()->format(DateTime::ISO8601);
				$data = array(
					"id_email_template" => $this->getEmailTemplate()->getId(),
					"id_user" => $this->getUser()->getId(),
					"sent" => (int)$this->getSent(),
					"sent_at" => $sent_at,
					"parse_successful" => (int)$this->getParseSuccessful(),
					"error" => (int)$this->getError(),
					"id" => $this->getId()
				);
				$sql = "UPDATE ".EmailUser::table." SET id_email_template = :id_email_template, id_user = :id_user, sent = :sent, sent_at = :sent_at, parse_successful = :parse_successful, error = :error WHERE id = :id";
				return Database::execute($sql, $data);
			}
			return false;
		}

		public static function addToQueue($id_email_template, $user, $data=null){
			if ($id_email_template && $user && $user->getId()){
				$email_user = EmailUser::create(array("email_template" => $id_email_template, "user" => $user->getId()));
				if ($email_user->getId() && $data && is_array($data)){
					foreach ($data as $obj){
						$reference_class = ReferenceClass::get($obj);
						if ($reference_class && $reference_class->getId()){
							$info = array(
								"id_email_user" => $email_user->getId(),
								"id_reference_class" => $reference_class->getId(),
								"object_id" => $obj->getId()
							);
							if (!EmailUserData::create($info)) return false;
						}else return false;
					}
					return $email_user;
				}
			}
			return true;
		}

		public static function validate($email_template, $user){
			$email_template = new EmailTemplate(intval($email_template));
			$user = new User(intval($user));
			if ($email_template->getId() && $user->getId()) return true;

			return false;
		}

		public function renderEmailFields($text, $email_fields){
			if (is_array($email_fields) && $text && strlen($text)>0){
				if (count($email_fields)>0){
					foreach ($email_fields as $email_field){
						$email_field_raw = str_replace('%', '', $email_field);
						list($class, $field) = explode('.', $email_field_raw);


						$reference_class = new ReferenceClass($class);

						$value = null;
						if ($reference_class->getId()){
							$data = EmailUserData::get($this, $reference_class);
							if ($data && $data->getId()) $value = $data->render($field);
						}

						if ($value){
							$text = str_replace($email_field, $value, $text);
						}else{
							print $email_field;
							return false;
						}
					}
				}
				return $text;
			}
			return false;
		}

		public static function process(){
			$sql_email_user = "SELECT id FROM ".EmailUser::table." WHERE sent = 'f' AND error = 'f' ORDER BY created_at ASC LIMIT 10 OFFSET 0";
			$email_user_list = Database::execute($sql_email_user)->fetchAll();

			if ($email_user_list && count($email_user_list)>0){
				foreach ($email_user_list as $email_user_id){
					$email_user = new EmailUser(intval($email_user_id["id"]));

					/* Get email Subject, Html Body and Text Body */
					$email_subject = $email_user->getEmailTemplate()->getSubject();
					$email_html_body = $email_user->getEmailTemplate()->getHTML();
					$email_text_body = $email_user->getEmailTemplate()->getTextBody();

					/* Find all tokens in very email element */
					$subject_tokens = preg_match_all('/%.[^\s]+%/', $email_subject, $subject_tokens_match);
					$html_tokens = preg_match_all('/%.[^\s]+%/', $email_html_body, $html_tokens_match);
					$text_tokens = preg_match_all('/%.[^\s]+%/', $email_text_body, $text_tokens_match);


					$email_subject = $email_user->renderEmailFields($email_subject, $subject_tokens_match[0]);

					$email_html_body = $email_user->renderEmailFields($email_html_body, $html_tokens_match[0]);

					$email_text_body = $email_user->renderEmailFields($email_text_body, $text_tokens_match[0]);

					if ($email_subject && $email_html_body && $email_text_body) {
						$email_user->setParseSuccessful(true);
						$mail = new Mail($email_subject, $email_html_body, $email_text_body);
						$mail->addAddress($email_user->getUser()->getName(), $email_user->getUser()->getEmail());

						if ($mail->send()){
							$email_user->setSent(true);
							$email_user->setSentAt(new DateTime("now"));
							if (!$email_user->update()) Log::error("PBA [500] FATAL: EmailUser[".$email_user->getId()."] Could not update sent info!");
						}else{
							$email_user->setError($true);
							if (!$email_user->update()) Log::error("PBA [500] FATAL: EmailUser[".$email_user->getId()."] Could not update send error info!");
							Log::error("PBA [500] FATAL: EmailUser[".$email_user->getId()."] Could not send Email User!");
						}
					}else{
						$email_user->setParseSuccessful(false);
						$email_user->setError(true);
						if (!$email_user->update()) Log::error("PBA [500] FATAL: EmailUser[".$email_user->getId()."] Could not update parse error info!");
					}
				}
			}

		}

		private function setId($id){
			$this->id = $id;
		}
		public function getId(){
			return $this->id;
		}

		public function setEmailTemplate($email_template){
			$this->email_template = new EmailTemplate($email_template);
		}
		public function getEmailTemplate(){
			return $this->email_template;
		}

		public function setUser($user){
			$this->user = new User($user);
		}
		public function getUser(){
			return $this->user;
		}

		public function setSent($sent){
			$this->sent = (bool)$sent;
		}
		public function getSent(){
			return (bool)$this->sent;
		}

		public function setSentAt($sent_at){
			$this->sent_at = new Datetime($sent_at);
		}
		public function getSentAt(){
			return $this->sent_at;
		}

		public function setParseSuccessful($parse_successful){
			$this->parse_successful = (bool)$parse_successful;
		}
		public function getParseSuccessful(){
			return (bool)$this->parse_successful;
		}

		public function setError($error){
			$this->error = (bool)$error;
		}
		public function getError(){
			return (bool)$this->error;
		}


		public function setCreatedAt($created_at){
			$this->created_at = new Datetime($created_at);
		}
		public function getCreatedAt($created_at){
			return $this->created_at;
		}
	}
?>
