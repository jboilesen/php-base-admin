<?php
	/*
	 * PBA - Copyright (c) 2011-2019 Jonathan Nunes Boilesen
	 *
	 *
	 * This software is Open Software.
	 *	This software is licensed under Apache License 2.0.
	 *
	 *
	 * author: Jonathan Nunes Boilesen
	 * date: 08/01/2016
	 */
	class Notification{
		const table = "app.notification";

		/* Notification type*/
		const system = 1;

		/* Notification per se */
		const new_notification_id = 1;


		private $id;
		private $title;
		private $description;
		private $type;
		private $email_template;
		private $created_at;

		public function __construct($id=null){
			if (is_int($id) && $id>0){
				$sql = "SELECT * FROM ".Notification::table." WHERE id = :id";
				$notification = Database::execute($sql, array("id" => $id))->fetch();
				$this->setId($notification["id"]);
				$this->setTitle($notification["title"]);
				$this->setDescription($notification["description"]);
				$this->setType($notification["type"]);
				$this->setEmailTemplate($notification["id_email_template"]);
				$this->setCreatedAt($notification["created_at"]);
			}
		}

		private function setId($id){
			$this->id = $id;
		}
		public function getId(){
			return $this->id;
		}

		public function getTitle(){
			return $this->title;
		}
		public function setTitle($title){
			$this->title = $title;
		}

		public function getDescription(){
			return $this->description;
		}
		public function setDescription($description){
			$this->description = $description;
		}

		public function getType(){
			return $this->type;
		}
		public function setType($type){
			$this->type = $type;
		}

		public function getEmailTemplate(){
			return $this->email_template;
		}
		public function setEmailTemplate($email_template){
			$this->email_template = new EmailTemplate($email_template);
		}

		public function getCreatedAt(){
			return $this->created_at;
		}
		public function setCreatedAt($created_at){
			$this->created_at = new Datetime($created_at);
		}
	}
?>
