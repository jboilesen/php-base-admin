<?php
	/*
	 * PBA - Copyright (c) 2011-2019 Jonathan Nunes Boilesen
	 *
	 *
	 * This software is Open Software.
	 *	This software is licensed under Apache License 2.0.
	 *
	 *
	 * author: Jonathan Nunes Boilesen
	 * date: 08/01/2016
	 */
  class ReferenceClassMethod{
    const table = "app.reference_class_method";

    private $id;
    private $id_reference_class;
    private $title;
    private $description;
    private $name;
    private $created_at;

    public function __construct($id_reference_class, $name){
      if ($id_reference_class!=null && $name!=null){
        $sql = "SELECT * FROM ".ReferenceClassMethod::table." WHERE id_reference_class = :id_reference_class AND name = :name";
        $reference_class_method = Database::execute($sql, array("id_reference_class" => $id_reference_class, "name" => $name))->fetch();
        $this->setId($reference_class_method["id"]);
        $this->setIdReferenceClass($reference_class_method["id_reference_class"]);
        $this->setTitle($reference_class_method["title"]);
        $this->setDescription($reference_class_method["description"]);
        $this->setName($reference_class_method["name"]);
        $this->setCreatedAt($reference_class_method["created_at"]);
      }
    }

    private function setId($id){
      $this->id = $id;
    }
    public function getId(){
      return $this->id;
    }

    public function getIdReferenceClass(){
      return $this->id_reference_class;
    }
    private function setIdReferenceClass($id_reference_class){
      $this->id_reference_class = $id_reference_class;
    }

    public function getTitle(){
      return $this->title;
    }
    public function setTitle($title){
      $this->title = $title;
    }

    public function getDescription(){
      return $this->description;
    }
    public function setDescription($description){
      $this->description = $description;
    }

    public function getName(){
      return $this->name;
    }
    public function setName($name){
      $this->name = $name;
    }

    public function getCreatedAt(){
      return $this->created_at;
    }
    public function setCreatedAt($created_at){
      $this->created_at = new Datetime($created_at);
    }

  }
?>
