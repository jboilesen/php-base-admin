<?php
	/*
	 * PBA - Copyright (c) 2011-2019 Jonathan Nunes Boilesen
	 *
	 *
	 * This software is Open Software.
	 *	This software is licensed under Apache License 2.0.
	 *
	 *
	 * author: Jonathan Nunes Boilesen
	 * date: 08/01/2016
	 * description: This pbaBackup.php is the pba backup script.
	 */
	/* PBA Config */
	require dirname(__FILE__).'/../pba.config.php';

	/* Composer Autoload */
	require $_ENV["root"].'/vendor/autoload.php';

	require $_ENV["root"].'/bootstrap/app.boot.php';

	if (!file_exists($_ENV["backup_dir"])) {
		mkdir($_ENV["backup_dir"], 0770, true);
		mkdir($_ENV["backup_dir"].'/db/', 0770, true);
	}

	Spatie\DbDumper\Databases\PostgreSql::create()
		->setDbName($_ENV["PBA_DATABASE_NAME"])
		->setUserName($_ENV["PBA_DATABASE_USER"])
		->setPassword($_ENV["PBA_DATABASE_PASSWORD"])
		->dumpToFile($_ENV["backup_dir"].'/db/'.$_ENV["PBA_DATABASE_NAME"]."_".date("Y_m_d_H_i_s").'.sql');

	copy($_ENV["root"].'/pba.config.php', $_ENV["backup_dir"].'/pba.config.php');

	/* Backing Up uploaded images */
	// Get real path for our folder
	$rootPath = realpath($_ENV["PBA_IMAGE_UPLOAD_PATH"]);

	// Initialize archive object
	$zip = new ZipArchive();
	$zip->open($_ENV["backup_dir"].'/images.zip', ZipArchive::CREATE | ZipArchive::OVERWRITE);

	// Create recursive directory iterator
	/** @var SplFileInfo[] $files */
	$files = new RecursiveIteratorIterator(
		new RecursiveDirectoryIterator($rootPath),
		RecursiveIteratorIterator::LEAVES_ONLY
	);

	foreach ($files as $name => $file){
		// Skip directories (they would be added automatically)
		if (!$file->isDir()){
			// Get real and relative path for current file
			$filePath = $file->getRealPath();
			$relativePath = substr($filePath, strlen($rootPath) + 1);

			// Add current file to archive
			$zip->addFile($filePath, $relativePath);
		}
	}

	// Zip archive will be created only after closing object
	$zip->close();

?>
