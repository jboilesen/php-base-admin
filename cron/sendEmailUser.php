<?php
	/*
	 * PBA - Copyright (c) 2011-2019 Jonathan Nunes Boilesen
	 *
	 *
	 * This software is Open Software.
	 *	This software is licensed under Apache License 2.0.
	 *
	 *
	 * author: Jonathan Nunes Boilesen
	 * date: 08/01/2016
	 * description: This sendEmailUser.php is used to send our users emails
	 */
	/* PBA Config */
	require dirname(__FILE__).'/../pba.config.php';

	/* Composer Autoload */
	require $_ENV["root"].'/vendor/autoload.php';

	/* PBA API Boot */
	require $_ENV["root"].'/bootstrap/app.boot.php';

	/* PBA Classes: your class goes here */
	require $_ENV["root"].'/classes/ReferenceClass.php';
	require $_ENV["root"].'/classes/ReferenceClassField.php';
	require $_ENV["root"].'/classes/ReferenceClassMethod.php';
	require $_ENV["root"].'/classes/Data.php';
	require $_ENV["root"].'/classes/Image.php';
	require $_ENV["root"].'/classes/User.php';
	require $_ENV["root"].'/classes/EmailUser.php';
	require $_ENV["root"].'/classes/EmailUserData.php';
	require $_ENV["root"].'/classes/EmailTemplate.php';
	require $_ENV["root"].'/classes/Notification.php';
	require $_ENV["root"].'/classes/UserNotification.php';
	require $_ENV["root"].'/classes/UserNotificationData.php';

	set_time_limit(500);

	EmailUser::process();

?>
