<?php
	/*
	 * PBA - Copyright (c) 2011-2019 Jonathan Nunes Boilesen
	 *
	 *
	 * This software is Open Software.
	 *	This software is licensed under Apache License 2.0.
	 *
	 *
	 * author: Jonathan Nunes Boilesen
	 * date: 08/01/2016
	 * description: This Gitlab.php is a simple Gitlab api wrapper
	 */
	class Gitlab {
		private static $connection = null;

		public static function connect(){
			try {
				if (self::$connection == null){
					self::$connection = \Gitlab\Client::create($_ENV["PBA_GITLAB_API_URL"]);
					self::$connection->authenticate($_ENV["PBA_GITLAB_API_AUTH"], \Gitlab\Client::AUTH_URL_TOKEN);
				}
			}catch (Exception $e){
				Log::cronError("[GITLAB] ".$e->getMessage());
				self::$connection = false;
			}
			return self::$connection;
		}

		public static function createUser($email, $password, $params){
			$user = false;
			try {
				$user = \Gitlab\Model\User::create(self::$connection, $email, $password, $params);
			}catch (Exception $e){
				Log::cronError("[GITLAB] PBA Fatal: Create user failed | [".$e->getCode()."] ".$e->getMessage());
			}
			return $user;
		}

		public static function createGroup($name, $path){
			$group = false;
			try{
				$group = \Gitlab\Model\Group::create(self::$connection, $name, $path);
			}catch (Exception $e){
				Log::cronError("[GITLAB] PBA Fatal: Create group failed | [".$e->getCode()."] ".$e->getMessage());
			}
			return $group;
		}

		public static function groupAddMember($group, $user_id, $access_level){
			try{
				$group->addMember($user_id, $access_level);
			}catch (Exception $e){
				Log::cronError("[GITLAB] PBA Fatal: Add user to group failed | [".$e->getCode()."] ".$e->getMessage());
				return false;
			}
			return true;
		}

		/*
		 * Projects
		 */
		public static function project($id){
			$project = null;
			try {
				$project = new \Gitlab\Model\Project($id, self::$connection);
			} catch (Exception $e) {
				Log::cronError("[GITLAB] PBA Fatal: Get project failed | [".$e->getCode()."] ".$e->getMessage());
				return false;
			}
			return $project;
		}

		public static function listProjects(){
			$projects = null;
			try {
				$projects = self::$connection->api('projects')->all(1, 100);
			} catch (Exception $e) {
				Log::cronError("[GITLAB] PBA Fatal: List projects failed | [".$e->getCode()."] ".$e->getMessage());
				return false;
			}
			return $projects;
		}
		public static function createProject($name, $params){
			$project = false;
			try{
				$project = \Gitlab\Model\Project::create(self::$connection, $name, $params);
			}catch (Exception $e){
				Log::cronError("[GITLAB] PBA Fatal: Create project failed | [".$e->getCode()."] ".$e->getMessage());
			}
			return $project;
		}

		public static function setProjectGroup($project, $group){
			try{
				$group->transfer($project->id);
			}catch (Exception $e){
				Log::cronError("[GITLAB] PBA Fatal: Transfer project failed | [".$e->getCode()."] ".$e->getMessage());
				return false;
			}
			return true;
		}


		/*
		 * Milestones
		 */
		public static function milestone($project, $milestone_id){
			$milestone = null;
 			try {
 				$milestone = $project->milestone($milestone_id);
 			} catch (Exception $e) {
 				Log::cronError("[GITLAB] PBA Fatal: Get milestone failed | [".$e->getCode()."] ".$e->getMessage());
 				return false;
 			}
 			return $milestone;
 		}
		public static function createMilestone($project, $title, $params){
			$milestone = false;
			try{
				$milestone = $project->createMilestone($title, $params);
			}catch (Exception $e){
				Log::cronError("[GITLAB] PBA Fatal: Create milestone failed | [".$e->getCode()."] ".$e->getMessage());
			}
			return $milestone;
		}

		/*
		 * Issues
		 */
		public static function createIssue($project, $title, $params){
			$issue = false;
			try{
				$issue = $project->createIssue($title, $params);
			}catch (Exception $e){
				Log::cronError("[GITLAB] PBA Fatal: Create issue failed | [".$e->getCode()."] ".$e->getMessage());
			}
			return $issue;
		}
	}
?>
