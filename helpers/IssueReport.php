<?php
	/*
	 * PBA - Copyright (c) 2011-2019 Jonathan Nunes Boilesen
	 *
	 *
	 * This software is Open Software.
	 *	This software is licensed under Apache License 2.0.
	 *
	 *
	 * author: Jonathan Nunes Boilesen
	 * date: 08/01/2016
	 * description: This IssueReport.php is a simple issue reporting class to help
	 * users to report bugs and ask for new features
	 */
  class IssueReport{
    public static function new($user, $type, $title, $description){
      if ($user && $user->getId() && $type && $title && $description){
        Gitlab::connect();
        $project = Gitlab::project($_ENV["PBA_ISSUE_REPORT_GITLAB_PROJECT_ID"]);
        $milestone = Gitlab::milestone($project, $_ENV["PBA_ISSUE_REPORT_GITLAB_MILESTONE_ID"]);
        if ($project && $milestone){
          $description.= "\n\n### User Fingerprint\nServer: ".$_ENV["hostname"]."\n\nUser Id: ".$user->getId()."\n\nUsername: ".$user->getUsername()."\n\nUser Name: ".$user->getName()."\n\nUser Email:".$user->getEmail();
        	$params = array(
        		"description" => $description,
        		"milestone_id" => $milestone->id,
            "labels" => $type
        	);
        	return Gitlab::createIssue($project, $title, $params);
        }
      }
      return false;
    }
  }
?>
