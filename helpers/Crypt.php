<?php
	/*
	 * PBA - Copyright (c) 2011-2019 Jonathan Nunes Boilesen
	 *
	 *
	 * This software is Open Software.
	 *	This software is licensed under Apache License 2.0.
	 *
	 *
	 * author: Jonathan Nunes Boilesen
	 * date: 08/01/2016
	 * description: This Crypt.php is a simple Random String or Number generator
	 */
	class Crypt {
		public static function random_string($length=10) {
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$characters_length = strlen($characters);
			$random_string = '';
			for ($i = 0; $i < $length; $i++) {
				$random_string .= $characters[rand(0, $characters_length - 1)];
			}
			return $random_string;
		}

		public static function random_number_string($length=10){
			$characters = '0123456789';
			$characters_length = strlen($characters);
			$random_string = '';
			for ($i = 0; $i < $length; $i++) {
				$random_string .= $characters[rand(0, $characters_length - 1)];
			}
			return $random_string;
		}
	}

?>
