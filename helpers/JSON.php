<?php
	/*
	 * PBA - Copyright (c) 2011-2019 Jonathan Nunes Boilesen
	 *
	 *
	 * This software is Open Software.
	 *	This software is licensed under Apache License 2.0.
	 *
	 *
	 * author: Jonathan Nunes Boilesen
	 * date: 08/01/2016
	 * description: This JSON.php is a simple JSON messages generator.
	 */
	class JSON {
		public static function success($csrf_name=null, $csrf_value=null, $data=null) {
			if ($csrf_name==null && $csrf_value==null){
				$response = array(
					"success" => true
				);
				return json_encode($response);
			}else if (strlen($csrf_name)>0 && strlen($csrf_value)>0){
				if ($data !== null && is_array($data)){
					$response = array(
						"success" => true,
						"data" => $data,
						"csrf" => array("name" => $csrf_name, "value" => $csrf_value)
					);
				}else{
					$response = array(
						"success" => true,
						"csrf" => array("name" => $csrf_name, "value" => $csrf_value)
					);
				}
				return json_encode($response);
			}
			return false;
		}
		public static function fail($csrf_name=null, $csrf_value=null, $data=null) {
			if ($csrf_name==null && $csrf_value==null){
				$response = array(
					"success" => false
				);
				return json_encode($response);
			}else if (strlen($csrf_name)>0 && strlen($csrf_value)>0){
				if ($data != null && is_array($data)){
					$response = array(
						"success" => false,
						"data" => $data,
						"csrf" => array("name" => $csrf_name, "value" => $csrf_value)
					);
				}else{
					$response = array(
						"success" => false,
						"csrf" => array("name" => $csrf_name, "value" => $csrf_value)
					);
				}
				return json_encode($response);
			}
			return false;
		}
	}

?>
