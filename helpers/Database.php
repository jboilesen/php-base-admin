<?php
	/*
	 * PBA - Copyright (c) 2011-2019 Jonathan Nunes Boilesen
	 *
	 *
	 * This software is Open Software.
	 *	This software is licensed under Apache License 2.0.
	 *
	 *
	 * author: Jonathan Nunes Boilesen
	 * date: 08/01/2016
	 * description: This Database.pba.config.php is a simple database connector
	 */
	class Database{
		private static $connection = null;

		public static function connect(){
			try {
				if (self::$connection == null){
					self::$connection = new PDO($_ENV["PBA_DATABASE_DSN"], $_ENV["PBA_DATABASE_USER"], $_ENV["PBA_DATABASE_PASSWORD"]);
				}
			}catch (Exception $e){
				Log::error("[DATABASE] ".$e->getMessage());
				self::$connection = false;
			}
			return self::$connection;
		}

		public static function execute($sql, $data=null){
			try{
				self::connect();
				$statement = self::$connection->prepare($sql);
				$result = $statement->execute($data);
				if (!$result){
					Log::error("[DATABASE] Error | [".$statement->errorInfo()[0]."] ".$statement->errorInfo()[1]." | ".$statement->errorInfo()[2]);
				}
			}catch(Exception $e){
				Log::error("[DATABASE] Exception | ".$e->getMessage());
				$statement = null;
			}
			if ($result) return $statement;

			return false;
		}
	}
?>
