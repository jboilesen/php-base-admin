<?php
	/*
	 * PBA - Copyright (c) 2011-2019 Jonathan Nunes Boilesen
	 *
	 *
	 * This software is Open Software.
	 *	This software is licensed under Apache License 2.0.
	 *
	 *
	 * author: Jonathan Nunes Boilesen
	 * date: 08/01/2016
	 * description: This v1.php creates all api paths for your project
	 */
/*
 * PBA API
 *
 */
$app->group('/api', function () use ($app, $container) {
	$app->group('/v1', function () use ($app, $container) {

		/*
		 * E-mail Methods
		 */
		$app->group('/email', function () use ($app, $container) {

			$app->group('/template', function () use ($app, $container){
				/*
				 * New E-mail Template
				 * [POST] /api/v1/email/template
				 */
				$app->post('', function ($request, $response, $args) use ($app){
					$response = $response->withHeader('Content-Type','application/json');
					$csrf_name = $request->getAttribute('csrf_name');
					$csrf_value = $request->getAttribute('csrf_value');
					try{
						if (isset($_SESSION["user"]) && $_SESSION["user"]->isAllowed(Permission::EMAIL_MANAGEMENT_CREATE)){
							$post = $request->getParsedBody();
							$email_template = EmailTemplate::create($post);
							if ($email_template && $email_template->getId()){
								$response->write(JSON::success($csrf_name,$csrf_value, $email_template->toArray()));
							}else{
								$response->write(JSON::fail($csrf_name,$csrf_value));
							}
						}else{
							Log::error("PBA [403] FATAL: POST api/v1/email | ".$e->getMessage());
							$response->setStatus(403)->headers->set('X-Status-Reason', 'Forbidden');
						}
					} catch (Exception $e) {
						Log::error("PBA [500] FATAL: POST api/v1/email | ".$e->getMessage());
						$response->setStatus(500)->headers->set('X-Status-Reason', 'Internal server error.');
					}
					return $response;
				})->add($container->get('csrf'));

				/*
				 * Edit E-mail Template
				 * [POST] /api/v1/email/template/edit
				 */
				$app->post('/edit', function ($request, $response, $args) use ($app) {
					$response = $response->withHeader('Content-Type','application/json');
					$csrf_name = $request->getAttribute('csrf_name');
					$csrf_value = $request->getAttribute('csrf_value');
					try{
						if (isset($_SESSION["user"]) && $_SESSION["user"]->isAllowed(Permission::EMAIL_MANAGEMENT_UPDATE)){
							$post = $request->getParsedBody();

							$email_template = null;
							if (isset($post["id"]) &&
								isset($post["title"]) &&
								isset($post["description"]) &&
								isset($post["subject"]) &&
								isset($post["html_body"]) &&
								isset($post["text_body"])){

								$email_template = new EmailTemplate(intval($post["id"]));
								$email_template->setTitle($post["title"]);
								$email_template->setDescription($post["description"]);
								$email_template->setSubject($post["subject"]);
								$email_template->setHtmlBody($post["html_body"]);
								$email_template->setTextBody($post["text_body"]);

								$email_template->setMarkdown(false);
								if (isset($post["markdown"])) $email_template->setMarkdown(true);
							}

							if ($email_template!=null && $email_template->getId()!=null && $email_template->update()){
								$response->write(JSON::success($csrf_name,$csrf_value, $email_template->toArray()));
							}else{
								$response->write(JSON::fail($csrf_name,$csrf_value));
							}
						}else{
							Log::error("PBA [403] FATAL: POST api/v1/email/template/edit | ".$e->getMessage());
							$response->setStatus(403)->headers->set('X-Status-Reason', 'Forbidden');
						}
					} catch (Exception $e) {
						Log::error("PBA [500] FATAL: POST api/v1/email/template/edit | ".$e->getMessage());
						$response->setStatus(500)->headers->set('X-Status-Reason', 'Internal server error.');
					}
					return $response;
				})->add($container->get('csrf'));

				$app->post('/list', function ($request, $response, $args) use ($app) {
					$response = $response->withHeader('Content-Type','application/json');
					$csrf_name = $request->getAttribute('csrf_name');
					$csrf_value = $request->getAttribute('csrf_value');
					try {
						if (isset($_SESSION["user"]) && $_SESSION["user"]->isAllowed(Permission::EMAIL_MANAGEMENT_READ)){
							$post = $request->getParsedBody();
							$email_templates = EmailTemplate::find();
							if (is_array($email_templates)){
								$email_templates_list = array();
								foreach ($email_templates as $i => $email_template){
									$email_templates_list[$i] = $email_template->toArray();
								}
								$response->write(JSON::success($csrf_name,$csrf_value, $email_templates_list));
							}else{
								$response->write(JSON::fail($csrf_name,$csrf_value));
							}
						}else{
							Log::error("PBA [403] FATAL: POST api/v1/email/template/list | ".$e->getMessage());
							$response->setStatus(403)->headers->set('X-Status-Reason', 'Forbidden');
						}
					} catch (Exception $e){
						Log::error("PBA [500] FATAL: POST api/v1/email/template/list | ".$e->getMessage());
						$response->setStatus(500)->headers->set('X-Status-Reason', 'Internal server error.');
					}
					return $response;
				})->add($container->get('csrf'));

				/*
				 *	E-mail Template Datatable
				 */
				$app->post('/datatable', function ($request, $response, $args) use ($app) {
					$response = $response->withHeader('Content-Type','application/json');
					$csrf_name = $request->getAttribute('csrf_name');
					$csrf_value = $request->getAttribute('csrf_value');
					try {
						if (isset($_SESSION["user"]) && $_SESSION["user"]->isAllowed(Permission::EMAIL_MANAGEMENT_READ)){
							$post = $request->getParsedBody();
							$data = EmailTemplate::dataTable($post);
							if ($data){
								$response->write(JSON::success($csrf_name,$csrf_value, $data));
							}else{
								$response->write(JSON::fail($csrf_name,$csrf_value));
							}
						}else{
							Log::error("PBA [403] FATAL: POST api/v1/email/template/datatable | ".$e->getMessage());
							$response->setStatus(403)->headers->set('X-Status-Reason', 'Forbidden');
						}
					} catch (Exception $e){
						Log::error("PBA [500] FATAL: POST api/v1/email/template/datatable | ".$e->getMessage());
						$response->setStatus(500)->headers->set('X-Status-Reason', 'Internal server error.');
					}
					return $response;
				})->add($container->get('csrf'));
			});
		});

		/*
		 * Image Methods
		 */
		$app->group('/image', function () use ($app, $container) {
			/*
			 * New Image
			 */
			$app->post('', function ($request, $response, $args) use ($app){
				$response = $response->withHeader('Content-Type','application/json');
				$csrf_name = $request->getAttribute('csrf_name');
				$csrf_value = $request->getAttribute('csrf_value');
				try{
					if (isset($_SESSION["user"])){
						$post = $request->getParsedBody();
						if (isset($request->getUploadedFiles()["image_file"])){
							$file = $request->getUploadedFiles()["image_file"];
							$image = Image::create($post, $file);
							if ($image && $image->getId()){
								$response->write(JSON::success($csrf_name, $csrf_value, $image->toArray()));
							}else{
								$response->write(JSON::fail($csrf_name,$csrf_value));
							}
						}else{
							Log::error("PBA [400] FATAL: POST api/v1/image | Unable to receive uploaded file");
							$response->setStatus(403)->headers->set('X-Status-Reason', 'Bad Request');
						}
					}else{
						Log::error("PBA [403] FATAL: POST api/v1/image | Unauthorized");
						$response->setStatus(403)->headers->set('X-Status-Reason', 'Forbidden');
					}
				} catch (Exception $e) {
					Log::error("PBA [500] FATAL: POST api/v1/image | ".$e->getMessage());
					$response->setStatus(500)->headers->set('X-Status-Reason', 'Internal server error.');
				}
				return $response;
			})->add($container->get('csrf'));

			/*
			 * Edit Image
			 */
			$app->post('/edit', function ($request, $response, $args) use ($app) {
				$response = $response->withHeader('Content-Type','application/json');
				$csrf_name = $request->getAttribute('csrf_name');
				$csrf_value = $request->getAttribute('csrf_value');
				try{
					if (isset($_SESSION["user"])){
						$post = $request->getParsedBody();
						$image = null;
						if (isset($post["id"]) && isset($post["title"]) && isset($post["description"])){
							$image = new Image(intval($post["id"]));
							$image->setTitle($post["title"]);
							$image->setDescription($post["description"]);
						}

						if ($image!=null && $image->getId()!=null && $image->update()){
							$response->write(JSON::success($csrf_name,$csrf_value));
						}else{
							$response->write(JSON::fail($csrf_name,$csrf_value));
						}
					}else{
						Log::error("PBA [403] FATAL: POST api/v1/image/edit | Unauthorized");
						$response->setStatus(403)->headers->set('X-Status-Reason', 'Forbidden');
					}
				} catch (Exception $e) {
					Log::error("PBA [500] FATAL: POST api/v1/image/edit | ".$e->getMessage());
					$response->setStatus(500)->headers->set('X-Status-Reason', 'Internal server error.');
				}
				return $response;
			})->add($container->get('csrf'));
			/*
			 * List Images
			 */
			$app->post('/list', function ($request, $response, $args) use ($app){
				$response = $response->withHeader('Content-Type','application/json');
				$csrf_name = $request->getAttribute('csrf_name');
				$csrf_value = $request->getAttribute('csrf_value');
				try{
					if (isset($_SESSION["user"])){
						$images = Image::find();
						if ($images && count($images)){
							$images_list = array();
							foreach ($images as $i => $image){
								$images_list[$i] = $image->toArray();
							}
							$response->write(JSON::success($csrf_name,$csrf_value, $images_list));
						}else{
							$response->write(JSON::fail($csrf_name,$csrf_value));
						}
					}else{
						Log::error("PBA [403] FATAL: POST api/v1/image/list | Unauthorized");
						$response->setStatus(403)->headers->set('X-Status-Reason', 'Forbidden');
					}
				} catch (Exception $e) {
					Log::error("PBA [500] FATAL: POST api/v1/image/list | ".$e->getMessage());
					$response->setStatus(500)->headers->set('X-Status-Reason', 'Internal server error.');
				}
				return $response;
			})->add($container->get('csrf'));

			/*
			 * Images Datatable
			 */
			$app->post('/datatable', function ($request, $response, $args) use ($app) {
				$response = $response->withHeader('Content-Type','application/json');
				$csrf_name = $request->getAttribute('csrf_name');
				$csrf_value = $request->getAttribute('csrf_value');
				try {
					if (isset($_SESSION["user"])){
						$post = $request->getParsedBody();
						$data = Image::dataTable($post);
						if ($data){
							$response->write(JSON::success($csrf_name,$csrf_value, $data));
						}else{
							$response->write(JSON::fail($csrf_name,$csrf_value));
						}
					}else{
						Log::error("PBA [403] FATAL: POST api/v1/image/datatable | Unauthorized");
						$response->setStatus(403)->headers->set('X-Status-Reason', 'Forbidden');
					}
				} catch (Exception $e){
					Log::error("PBA [500] FATAL: POST api/v1/image/datatable | ".$e->getMessage());
					$response->setStatus(500)->headers->set('X-Status-Reason', 'Internal server error.');
				}
				return $response;
			})->add($container->get('csrf'));
		});


		/*
		 * User Methods
		 */
		$app->group('/user', function () use ($app, $container) {

			/*
			 * User Authentication
			 */
			$app->group('/auth', function () use ($app, $container) {
				/*
				 * Check if User is authenticated
				 */
				$app->get('', function ($request, $response, $args) use ($app) {
					$response = $response->withHeader('Content-Type','application/json');
					$csrf_name = $request->getAttribute('csrf_name');
					$csrf_value = $request->getAttribute('csrf_value');
					if (isset($_SESSION["user"])){
						$response->write(JSON::success($csrf_name,$csrf_value));
					}else{
						$response->write(JSON::fail($csrf_name,$csrf_value));
					}
					return $response;

				})->add($container->get('csrf'));

				/*
				 * Authenticate User
				 */
				$app->post('', function ($request, $response, $args) use ($app){
					$response = $response->withHeader('Content-Type','application/json');
					$csrf_name = $request->getAttribute('csrf_name');
					$csrf_value = $request->getAttribute('csrf_value');
					try {
						$post = $request->getParsedBody();
						if (isset($post["username"]) && isset($post["password"])){
							if (User::authenticate($post["username"],$post["password"])){
								$response->write(JSON::success($csrf_name,$csrf_value));
							}else{
								$response->write(JSON::fail($csrf_name,$csrf_value));
							}
						}else{
							$response = $response>withStatus(400)->withAddedHeader('X-Status-Reason', 'Bad Request');
						}
					} catch (Exception $e) {
						Log::error("PBA [500] FATAL: api/v1/user/auth | ".$e->getMessage());
						$response = $response->withStatus(500)->withAddedHeader('X-Status-Reason', 'Internal server error.');
					}
					return $response;
				})->add($container->get('csrf'));
			});

			/*
			 * User Reset Password
			 */
			$app->group('/reset', function () use ($app, $container){
				/*
				 * Token Generates user a password reset token
				 */
				$app->post('/token', function ($request, $response, $args) use ($app){
					$response = $response->withHeader('Content-Type','application/json');
					$csrf_name = $request->getAttribute('csrf_name');
						$csrf_value = $request->getAttribute('csrf_value');
					try{
						if (!isset($_SESSION["user"])){
							$post = $request->getParsedBody();
							if (isset($post["email"]) && User::askResetPassword($post["email"])){
								$response->write(JSON::success($csrf_name,$csrf_value));
							}else{
								$response->write(JSON::fail($csrf_name,$csrf_value));
							}
						}else{
							$response = $response->withStatus(400)->withAddedHeader('X-Status-Reason', 'Bad Request');
						}
					} catch (Exception $e) {
						Log::error("PBA [500] FATAL: api/v1/user/reset/token | ".$e->getMessage());
						$response = $response->withStatus(500)->withAddedHeader('X-Status-Reason', 'Internal server error.');
					}
					return $response;
				})->add($container->get('csrf'));

				/*
				 * Change user password when reset token is presented
				 */
				$app->post('/password', function ($request, $response, $args) use ($app){
					$response = $response->withHeader('Content-Type','application/json');
					$csrf_name = $request->getAttribute('csrf_name');
						$csrf_value = $request->getAttribute('csrf_value');
					try{
						if (!isset($_SESSION["user"])){
							$post = $request->getParsedBody();
							if (isset($post["token"]) && isset($post["password"]) && User::resetPassword($post["token"], $post["password"])){
								$response->write(JSON::success($csrf_name,$csrf_value));
							}else{
								$response->write(JSON::fail($csrf_name,$csrf_value));
							}
						}else{
							$response = $response->withStatus(400)->withAddedHeader('X-Status-Reason', 'Bad Request');
						}
					} catch (Exception $e) {
						Log::error("PBA [500] FATAL: api/v1/user/reset/password | ".$e->getMessage());
						$response->setStatus(500)->headers->set('X-Status-Reason', 'Internal server error.');
					}
					return $response;
				})->add($container->get('csrf'));
			});

			/*
			 * Logout Authenticated User
			 */
			$app->post('/logout', function ($request, $response, $args) use ($app) {
				$response = $response->withHeader('Content-Type','application/json');
				$csrf_name = $request->getAttribute('csrf_name');
					$csrf_value = $request->getAttribute('csrf_value');
				if (isset($_SESSION["user"])){
					unset($_SESSION["user"]);
					session_destroy();
					$response->write(JSON::success($csrf_name,$csrf_value));
				}else{
					$response->write(JSON::fail($csrf_name,$csrf_value));
				}
				return $response;
			})->add($container->get('csrf'));

			/*
			 *  New User
			 */
			$app->post('', function ($request, $response, $args) use ($app){
				$response = $response->withHeader('Content-Type','application/json');
				$csrf_name = $request->getAttribute('csrf_name');
				$csrf_value = $request->getAttribute('csrf_value');
				try{
					if (isset($_SESSION["user"]) && $_SESSION["user"]->isAllowed(Permission::USER_MANAGEMENT_CREATE)){
						$post = $request->getParsedBody();
						$user = User::create($post);
						if ($user && $user->getId()){
							$response->write(JSON::success($csrf_name,$csrf_value,$user->toArray()));
						}else{
							$response->write(JSON::fail($csrf_name,$csrf_value));
						}
					}else{
						Log::error("PBA [403] FATAL: POST api/v1/user | Unauthorized");
						$response->setStatus(403)->headers->set('X-Status-Reason', 'Forbidden');
					}
				} catch (Exception $e) {
					Log::error("PBA [500] FATAL: POST api/v1/user | ".$e->getMessage());
					$response->setStatus(500)->headers->set('X-Status-Reason', 'Internal server error.');
				}
				return $response;
			})->add($container->get('csrf'));

			/*
			 * Edit User
			 */
			$app->post('/edit', function ($request, $response, $args) use ($app) {
				$response = $response->withHeader('Content-Type','application/json');
				$csrf_name = $request->getAttribute('csrf_name');
				$csrf_value = $request->getAttribute('csrf_value');
				try{
					if (isset($_SESSION["user"])){
						$post = $request->getParsedBody();

						if (isset($post["id"]) &&
							($_SESSION["user"]->isAllowed(Permission::USER_MANAGEMENT_UPDATE) ||
							($_SESSION["user"]->isAllowed(Permission::USER_UPDATE) && $post["id"] == $_SESSION["user"]->getId()))){
								$user = null;
								if (isset($post['id']) &&
									isset($post['name']) &&
									isset($post['email']) &&
									isset($post['username'])){
									$user = new User(intval($post["id"]));
									$user->setName($post["name"]);
									$user->setEmail($post["email"]);
									$user->setUsername($post["username"]);

									if (isset($post["password"]) && !empty($post["password"])) $user->setPassword(password_hash($post["password"], PASSWORD_BCRYPT));

									if (isset($post["profile_picture"])) $user->setProfilePicture(intval($post["profile_picture"]));

									if (isset($post["bio"])) $user->setBio($post["bio"]);

									if (isset($post["linkedin"])) $user->setLinkedin($post["linkedin"]);

									if (isset($post["facebook"])) $user->setFacebook($post["facebook"]);

									if (isset($post["twitter"])) $user->setTwitter($post["twitter"]);

									if (isset($post["youtube"])) $user->setYoutube($post["youtube"]);

									$user->setNotifySystemEvents(false);
									if (isset($post["notify_system_events"])) $user->setNotifySystemEvents($post["notify_system_events"]);
								}

								if ($user!=null && $user->getId()!=null && $user->update()){
									/* If altered user was the logged in user, update it */
									if ($_SESSION["user"]->getId() == $user->getId()) $_SESSION["user"] = $user;
									$response->write(JSON::success($csrf_name,$csrf_value));
								}else{
									$response->write(JSON::fail($csrf_name,$csrf_value));
								}
						}else{
							Log::error("PBA [403] FATAL: POST api/v1/user/edit | Not allowed");
							$response->setStatus(403)->headers->set('X-Status-Reason', 'Forbidden');
						}
					}else{
						Log::error("PBA [403] FATAL: POST api/v1/user/edit | Unauthorized");
						$response->setStatus(403)->headers->set('X-Status-Reason', 'Forbidden');
					}
				} catch (Exception $e) {
					Log::error("PBA [500] FATAL: POST api/v1/user/edit | ".$e->getMessage());
					$response->setStatus(500)->headers->set('X-Status-Reason', 'Internal server error.');
				}
				return $response;
			})->add($container->get('csrf'));

			/*
			 * User Datatable
			 */
			$app->post('/datatable', function ($request, $response, $args) use ($app) {
				$response = $response->withHeader('Content-Type','application/json');
				$csrf_name = $request->getAttribute('csrf_name');
				$csrf_value = $request->getAttribute('csrf_value');
				try {
					if (isset($_SESSION["user"]) &&
						$_SESSION["user"]->isAllowed(Permission::USER_MANAGEMENT_READ)){

						$post = $request->getParsedBody();
						$data = User::dataTable($post);
						if ($data){
							$response->write(JSON::success($csrf_name,$csrf_value, $data));
						}else{
							$response->write(JSON::fail($csrf_name,$csrf_value));
						}
					}else{
						Log::error("PBA [403] FATAL: POST api/v1/user/datatable | Unauthorized");
						$response->setStatus(403)->headers->set('X-Status-Reason', 'Forbidden');
					}
				} catch (Exception $e){
					Log::error("PBA [500] FATAL: POST api/v1/user/datatable | ".$e->getMessage());
					$response->setStatus(500)->headers->set('X-Status-Reason', 'Internal server error.');
				}
				return $response;
			})->add($container->get('csrf'));

			/*
			 * Notification Methods POST /user/notification/read
			 */
			$app->group('/notification', function () use ($app, $container) {
				/*
				 *	Mark notification as read
				 */
				$app->post('/read', function ($request, $response, $args) use ($app){
					$response = $response->withHeader('Content-Type','application/json');
					$csrf_name = $request->getAttribute('csrf_name');
					$csrf_value = $request->getAttribute('csrf_value');
					try{
						if (isset($_SESSION["user"])){
							$post = $request->getParsedBody();
							if (isset($post["user_notification"])){
								$user_notification = new UserNotification(intval($post["user_notification"]));
								if ($user_notification->getId() && $_SESSION["user"]->getId() == $user_notification->getUser()->getId() && $user_notification->read()){
									$data = $user_notification->getData();
									if ($data && $data->getId()){
										$response->write(JSON::success($csrf_name,$csrf_value, $data->toArray()));
									}else{
										$response->write(JSON::fail($csrf_name,$csrf_value));
									}
								}else{
									$response->write(JSON::fail($csrf_name,$csrf_value));
								}
							}else{
								Log::error("PBA [400] FATAL: POST api/v1/user/notification/read ");
								$response->setStatus(400)->headers->set('X-Status-Reason', 'Bad request');
							}
						}else{
							Log::error("PBA [403] FATAL: POST api/v1/user/notification/read");
							$response->setStatus(403)->headers->set('X-Status-Reason', 'Forbidden');
						}
					} catch (Exception $e) {
						Log::error("PBA [500] FATAL: POST api/v1/user/notification/read | ".$e->getMessage());
						$response->setStatus(500)->headers->set('X-Status-Reason', 'Internal server error.');
					}
					return $response;
				})->add($container->get('csrf'));
			});
		});

		 /*
 		 * Report Issue Methods
 		 */
 		$app->group('/issue', function () use ($app, $container) {
 			/*
 			 * Choose report period
 			 */
 			$app->post('', function ($request, $response, $args) use ($app){
				$response = $response->withHeader('Content-Type','application/json');
 				$csrf_name = $request->getAttribute('csrf_name');
 				$csrf_value = $request->getAttribute('csrf_value');
 				try{
 					if (isset($_SESSION["user"])){
 						$post = $request->getParsedBody();
 						if (isset($post["type"]) &&
								isset($post["title"]) &&
								isset($post["description"])
								&& ($post["type"] == "Bug" || $post["type"] == "Feature")){
							$issue = IssueReport::new($_SESSION["user"], $post["type"], $post["title"], $post["description"]);
 							if ($issue){
 								$response->write(JSON::success($csrf_name,$csrf_value));
 							}else{
 								$response->write(JSON::fail($csrf_name,$csrf_value));
 							}
 						}else{
 							Log::error("PBA [400] FATAL: POST api/v1/issue ");
 							$response->setStatus(400)->headers->set('X-Status-Reason', 'Bad request');
 						}
 					}else{
 						Log::error("PBA [403] FATAL: POST api/v1/issue");
 						$response->setStatus(403)->headers->set('X-Status-Reason', 'Forbidden');
 					}
 				} catch (Exception $e) {
 					Log::error("PBA [500] FATAL: POST api/v1/issue | ".$e->getMessage());
 					$response->setStatus(500)->headers->set('X-Status-Reason', 'Internal server error.');
 				}
 				return $response;
 			})->add($container->get('csrf'));
 		});

		/*
		 * Add here your other entities methods
		 */
	});
});
?>
