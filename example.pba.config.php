<?php
	/*
	 * PBA - Copyright (c) 2011-2019 Jonathan Nunes Boilesen
	 *
	 *
	 * This software is Open Software.
	 *	This software is licensed under Apache License 2.0.
	 *
	 *
	 * author: Jonathan Nunes Boilesen
	 * date: 08/01/2016
	 * description: This example.pba.config.php is the PBA configuration file example.
	 * Copy this file to pba.config.php and make the necessary changes to configure your PBA.
	 */

/* PBA Environment Variables */

$_ENV["hostname"] = "http://dev.pba";
$_ENV["root"] = dirname(__FILE__);
$_ENV["backup_dir"] = $_ENV["root"].'/backup/pba';

$_ENV["PBA_APP_NAME"] = "PHP Base Admin";
$_ENV["PBA_APP_OWNER"] = "PHP Base Admin";

$_ENV["PBA_ADMIN_FOLDER"] = "/admin";
$_ENV["PBA_ADMIN_PAGES"] = $_ENV["root"].$_ENV["PBA_ADMIN_FOLDER"]."/pages";
$_ENV["PBA_ADMIN_IMAGE_FOLDER"] = $_ENV["PBA_ADMIN_FOLDER"]."/assets/images/";
$_ENV["PBA_IMAGE_FOLDER"] = "/assets/images/";
$_ENV["PBA_IMAGE_UPLOAD_PATH"] = $_ENV["PBA_ADMIN_IMAGE_FOLDER"];

/* PBA Debug:
	- false: no debug, info or notice log registry
	- true: debug, info and notice logging
*/
$_ENV["PBA_DEBUG"] = true;

$_ENV["PBA_MAINTENANCE"] = false;

/* PBA Environment:
	- development
	- testing
	- production
*/
$_ENV["PBA_ENV"] = "development";

/*
 * PBA Database Configuration
 */
$_ENV["PBA_DATABASE_USER"] = 'pba';
$_ENV["PBA_DATABASE_NAME"] = $_ENV["PBA_DATABASE_USER"]."_".$_ENV["PBA_ENV"];
$_ENV["PBA_DATABASE_SCHEMA"] = 'app';
$_ENV["PBA_DATABASE_PASSWORD"] = 'pba_password';
$_ENV["PBA_DATABASE_DSN"] = 'pgsql:host=localhost;dbname='.$_ENV["PBA_DATABASE_NAME"];

/*
 * PBA e-mail configuration
 */
$_ENV["PBA_MAIL_SMTP_HOST"] = "smtp.example.com";
$_ENV["PBA_MAIL_SMTP_USER"] = "app@example.com";
$_ENV["PBA_MAIL_NOTIFY"] = "admin@example.com";
$_ENV["PBA_MAIL_SMTP_PASSWORD"] = 'app_example_com_password';
$_ENV["PBA_MAIL_SMTP_ENCRYPTION"] ="tls";
$_ENV["PBA_MAIL_SMTP_PORT"] = 587;
$_ENV["PBA_MAIL_FROM_NAME"] = "PHP Base Admin";
$_ENV["PBA_MAIL_FROM_EMAIL"] = "app@example.com";
$_ENV["PBA_MAIL_REPLY_TO_NAME"] = "No Reply";
$_ENV["PBA_MAIL_REPLY_TO_EMAIL"] = "noreply@example.com";
$_ENV["PBA_MAIL_SIGNATURE"] = "PHP Base Admin";

/* Gitlab */
$_ENV["PBA_GITLAB_API_URL"] = "https://www.gitlab.com/api/v4/";
$_ENV["PBA_GITLAB_API_AUTH"] = "secret_gitlab_token";

/* Gitlab Bug Report */
$_ENV["PBA_ISSUE_REPORT_GITLAB_PROJECT_ID"] = 1; // put your bug tracking project id here
$_ENV["PBA_ISSUE_REPORT_GITLAB_MILESTONE_ID"] = 1; // put your bug tracking backlog milestone id here
?>
