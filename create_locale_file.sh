#!/bin/sh

count_aux = 0

get_all_text_subfolders() {
    for i in "$1"/*;do
        if [ -d "$i" ];then
            get_all_text_subfolders "$i"
        fi
        case "$i" in
          *.php )
                count_aux=$((count_aux+1))
                xgettext --default-domain=php-base-admin_$count_aux -p ./locale --from-code=UTF-8 -n --omit-header -L PHP $i
                ;;
          esac
    done
}

delete_po_files() {
    for i in "$1"/*.po;do
        if [ $i != "$1/php-base-admin.po" ];then
            rm $i
        fi
    done
}

# try get path from param
path=""
if [ -d "$1" ]; then
    path=$1;
else
    path="/home/samuel/Documentos/Projetos/php-base-admin"
fi

get_all_text_subfolders "$path/admin"
cmd = ""

#get all .po files
for i in "$path/locale"/*.po; do
    cmd="$cmd $i"
done

#merge all files
cat $cmd > "$path/locale/php-base-admin.po"

delete_po_files "$path/locale"

#make unique msgids
for cat in "$path/locale"/*.po; do
    msguniq --use-first $cat > /tmp/z.po
    mv /tmp/z.po $cat
done

#insert header
cat "$path/locale/po-header.txt" "$path/locale/php-base-admin.po" > /tmp/z.po
mv /tmp/z.po "$path/locale/php-base-admin.po"

