<?php
	/*
	 * PBA - Copyright (c) 2011-2019 Jonathan Nunes Boilesen
	 *
	 *
	 * This software is Open Software.
	 *	This software is licensed under Apache License 2.0.
	 *
	 *
	 * author: Jonathan Nunes Boilesen
	 * date: 08/01/2016
	 * description: This index.php is the file that will contain every HTTP path with its HTTP methods.
	 */

	/* PBA Config */
	require dirname(__FILE__).'/../pba.config.php';

	/* Composer Autoload */
	require $_ENV["root"].'/vendor/autoload.php';

	/* PBA API Boot */
	require $_ENV["root"].'/bootstrap/app.boot.php';

	/* PBA Classes: your class goes here */
	require $_ENV["root"].'/classes/ReferenceClass.php';
	require $_ENV["root"].'/classes/ReferenceClassField.php';
	require $_ENV["root"].'/classes/ReferenceClassMethod.php';
	require $_ENV["root"].'/classes/Data.php';
	require $_ENV["root"].'/classes/Scope.php';
	require $_ENV["root"].'/classes/Permission.php';
	require $_ENV["root"].'/classes/Role.php';
	require $_ENV["root"].'/classes/User.php';
	require $_ENV["root"].'/classes/Image.php';
	require $_ENV["root"].'/classes/Email.php';
	require $_ENV["root"].'/classes/EmailTemplate.php';
	require $_ENV["root"].'/classes/EmailUser.php';
	require $_ENV["root"].'/classes/EmailUserData.php';
	require $_ENV["root"].'/classes/Notification.php';
	require $_ENV["root"].'/classes/UserNotification.php';
	require $_ENV["root"].'/classes/UserNotificationData.php';

	/* PBA: Session start */
 	session_start();

	if ($_ENV["PBA_MAINTENANCE"]){
		$app->get('/[{path:.*}]', function ($request, $response, $args) use ($app) {
			require $_ENV["PBA_THEME_PAGES"].'/maintenance.php';
			return $response;
		});
	}else{


		/*
		 * System Basic Routes
		 */
		require $_ENV["root"].'/routes/system.php';

		/*
		 * API Routes
		 */
		require $_ENV["root"].'/routes/api/v1.php';

		/*
		 * Administration Routes
		 */
		require $_ENV["root"].$_ENV["PBA_ADMIN_FOLDER"].'/paths.php';

		/*
		 * Authenticated User Access Restriction
		 */
		$app->get('/[{path:.*}]', function ($request, $response, $args) use ($app) {
			if (isset($_SESSION["user"])){
				header("Location: ".$_ENV["hostname"]."/home");
			}else{
				header("Location:".$_ENV["hostname"]."/login");
			}
			return $response;
		})->add($container->get('csrf'));
	}

	$app->run();
?>
