<?php


use Phinx\Seed\AbstractSeed;

class AdminUserScopePermission extends AbstractSeed
{
	public function getDependencies(){
		return [
			'UserScopePermission'
		];
	}
		/**
		 * Run Method.
		 *
		 * Write your database seeder using this method.
		 *
		 * More information on writing seeders is available here:
		 * http://docs.phinx.org/en/latest/seeding.html
		 */
		public function run() {
			$this->getAdapter()->setOptions(array_replace($this->getAdapter()->getOptions(), ['schema' => 'app']));

			$role_permission = $this->table('role_permission');

			$role_permission_row_1 = array(
				"id_role" => 1,
				"id_permission" => 1
			);
			$role_permission->insert($role_permission_row_1);

			$role_permission_row_2 = array(
				"id_role" => 1,
				"id_permission" => 2
			);
			$role_permission->insert($role_permission_row_2);

			$role_permission_row_3 = array(
				"id_role" => 1,
				"id_permission" => 3
			);
			$role_permission->insert($role_permission_row_3);

			$role_permission_row_4 = array(
				"id_role" => 1,
				"id_permission" => 4
			);
			$role_permission->insert($role_permission_row_4);

			$role_permission_row_5 = array(
				"id_role" => 1,
				"id_permission" => 5
			);
			$role_permission->insert($role_permission_row_5)->saveData();
		}
}
