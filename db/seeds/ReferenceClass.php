<?php
	/*
	 * PBA - Copyright (c) 2011-2019 Jonathan Nunes Boilesen
	 *
	 *
	 * This software is Open Software.
	 *	This software is licensed under Apache License 2.0.
	 *
	 *
	 * author: Jonathan Nunes Boilesen
	 * date: 08/01/2016
	 * description: This seed populates our reference class table
	 */
use Phinx\Seed\AbstractSeed;

class ReferenceClass extends AbstractSeed {

		public function getDependencies(){
			return [
				'InitialDatabaseSeed'
			];
		}


		/**
		 * Run Method.
		 *
		 * Write your database seeder using this method.
		 *
		 * More information on writing seeders is available here:
		 * http://docs.phinx.org/en/latest/seeding.html
		 */
		public function run() {
			$this->getAdapter()->setOptions(array_replace($this->getAdapter()->getOptions(), ['schema' => 'app']));
			/* Reference Class */
			$reference_class = $this->table('reference_class');
			$reference_class_row_1 = array(
				"title" => "User",
				"description" => "Classe de gestão de usuários do sistema",
				"name" => "user"
			);
			$reference_class->insert($reference_class_row_1)->saveData();
		}
}
