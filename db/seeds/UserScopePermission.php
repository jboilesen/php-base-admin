<?php


use Phinx\Seed\AbstractSeed;

class UserScopePermission extends AbstractSeed
{
	public function getDependencies(){
		return [
			'ReferenceClassFields'
		];
	}
		/**
		 * Run Method.
		 *
		 * Write your database seeder using this method.
		 *
		 * More information on writing seeders is available here:
		 * http://docs.phinx.org/en/latest/seeding.html
		 */
		public function run() {
			$this->getAdapter()->setOptions(array_replace($this->getAdapter()->getOptions(), ['schema' => 'app']));

			$scope = $this->table('scope');
			$scope_row_1 = array(
				"name" => "User",
				"description" => "Own user methods scope"
			);
			$scope->insert($scope_row_1);

			$scope_row_2 = array(
				"name" => "User Management",
				"description" => "User management methods scope"
			);
			$scope->insert($scope_row_2)->saveData();

			$permission = $this->table('permission');
			$permission_row_1 = array(
				"name" => "Read",
				"description" => "Read own data",
				"id_scope" => 1
			);
			$permission->insert($permission_row_1);

			$permission_row_2 = array(
				"name" => "Update",
				"description" => "Update own user data",
				"id_scope" => 1
			);
			$permission->insert($permission_row_2);

			$permission_row_3 = array(
				"name" => "Create",
				"description" => "Create new users",
				"id_scope" => 2
			);
			$permission->insert($permission_row_3);

			$permission_row_4 = array(
				"name" => "Read",
				"description" => "Read users data",
				"id_scope" => 2
			);
			$permission->insert($permission_row_4);

			$permission_row_5 = array(
				"name" => "Update",
				"description" => "Update users data",
				"id_scope" => 2
			);
			$permission->insert($permission_row_5)->saveData();
		}
}
