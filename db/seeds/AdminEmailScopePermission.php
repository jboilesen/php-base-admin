<?php


use Phinx\Seed\AbstractSeed;

class AdminEmailScopePermission extends AbstractSeed
{
	public function getDependencies(){
		return [
			'EmailScopePermission'
		];
	}
		/**
		 * Run Method.
		 *
		 * Write your database seeder using this method.
		 *
		 * More information on writing seeders is available here:
		 * http://docs.phinx.org/en/latest/seeding.html
		 */
		public function run() {
			$this->getAdapter()->setOptions(array_replace($this->getAdapter()->getOptions(), ['schema' => 'app']));

			$role_permission = $this->table('role_permission');

			$role_permission_row_1 = array(
				"id_role" => 1,
				"id_permission" => 6
			);
			$role_permission->insert($role_permission_row_1);

			$role_permission_row_2 = array(
				"id_role" => 1,
				"id_permission" => 7
			);
			$role_permission->insert($role_permission_row_2);

			$role_permission_row_3 = array(
				"id_role" => 1,
				"id_permission" => 8
			);
			$role_permission->insert($role_permission_row_3)->saveData();
		}
}
