<?php
	/*
	 * PBA - Copyright (c) 2011-2019 Jonathan Nunes Boilesen
	 *
	 *
	 * This software is Open Software.
	 *	This software is licensed under Apache License 2.0.
	 *
	 *
	 * author: Jonathan Nunes Boilesen
	 * date: 08/01/2016
	 * description: This migration creates the app.reference_class table
	 */

use Phinx\Migration\AbstractMigration;

class ReferenceClassMethod extends AbstractMigration
{
		/**
		 * Change Method.
		 *
		 * Write your reversible migrations using this method.
		 *
		 * More information on writing migrations is available here:
		 * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
		 *
		 * The following commands can be used in this method and Phinx will
		 * automatically reverse them when rolling back:
		 *
		 *		createTable
		 *		renameTable
		 *		addColumn
		 *		renameColumn
		 *		addIndex
		 *		addForeignKey
		 *
		 * Remember to call "create()" or "update()" and NOT "save()" when working
		 * with the Table class.
		 */
		public function change(){
			$this->getAdapter()->setOptions(array_replace($this->getAdapter()->getOptions(), ['schema' => 'app']));

			$reference_class_method = $this->table('reference_class_method');
			$reference_class_method->addColumn('id_reference_class', 'integer', ['null' => false])
				->addColumn('title', 'string', ['limit' => 255,'null' => false])
				->addColumn('description', 'string', ['limit' => 500, 'null' => false])
				->addColumn('name', 'string', ['limit' => 255, 'null' => false])
				->addIndex(['id_reference_class','name'], ['unique' => true])
				->addColumn('created_at', 'timestamp', ['timezone' => true, 'default' => 'CURRENT_TIMESTAMP', 'null' => false])
				->save();
			$reference_class_method->addForeignKey(['id_reference_class'], 'reference_class', 'id', ['delete' => 'RESTRICT', 'update' => 'CASCADE'])->save();
		}
}
